﻿using System;

using System.Collections.Generic;

using System.Linq;

using System.Text;



namespace Herencia

{

    public class Persona

    {

        protected int cedula;

        protected string nombre;

        protected string apellido;

        protected int edad;



        public int Cedula

        {

            set

            {

                cedula = value;

            }

            get

            {

                return cedula;

            }

        }

        public string Nombre

        {

            set

            {

                nombre = value;

            }

            get

            {

                return nombre;

            }

        }

        public string Apellido

        {

            set

            {

                apellido = value;

            }

            get

            {

                return apellido;

            }

        }



        public int Edad

        {

            set

            {

                edad = value;

            }

            get

            {

                return edad;

            }

        }





        public void Imprimir()

        {



            Console.WriteLine("Nombre: " + Nombre);

            Console.WriteLine("Apellido: " + Apellido);

            Console.WriteLine("Edad: " + Edad);

            Console.WriteLine("Cedula: " + Cedula);

        }

    }



    public class Profesor : Persona

    {

        protected float sueldo;



        public float Sueldo

        {

            set

            {

                sueldo = value;

            }

            get

            {

                return sueldo;

            }

        }



        new public void Imprimir()

        {

            base.Imprimir();

            Console.WriteLine("Sueldo: " + Sueldo);

        }

    }



    class Prueba

    {

        static void Main(string[] args)

        {

            Persona persona1 = new Persona();

            persona1.Nombre = "Juan";

            persona1.Apellido = "Lorenzo";

            persona1.Edad = 25;

            persona1.Cedula = 1392573;

            Console.WriteLine("Los datos de la persona son: ");

            persona1.Imprimir();



            Profesor empleado1 = new Profesor();

            empleado1.Nombre = "Maria";

            empleado1.Apellido = "Jimenez";

            empleado1.Edad = 42;

            empleado1.Cedula = 1681523;

            empleado1.Sueldo = 2524;

            Console.WriteLine("Los datos del profesor son: ");

            empleado1.Imprimir();



            Console.ReadKey();

        }

    }

}
